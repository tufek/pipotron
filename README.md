# Utilisation du pipotron

## Prérequis
Avoir git installé
`$ sudo apt update && sudo apt -y install git`

## Cloner le dossier
`$ git clone https://gitlab.com/tufek/pipotron.git`

## Mode manuel (avec main_manuel.sh)

  1. rendre exécutable pipotron et main_manuel: `$ chmod a+x main_manuel.sh pipotron.sh`
  2. exécuter main_manuel: `$ ./main_manuel.sh`

Les résultats s'affichent dans le terminal
## Mode automatique (avec main.sh et le fichier import.csv):

  1. Complétez le fichier import.csv (séparateurs: virgules)
  2. rendre exécutable main.sh: `$ chmod +x main.sh`
  3. exécuter main.sh `$ ./main.sh`

Les résultats sont situés dans resultats.txt dans le dossier source.
