#!/usr/bin/env bash
#préparation du fichier csv (remplacement des virgules par des espaces)
cat `pwd`/import.csv | sed 1d | tr "," " " > app_tmp.csv
#lecture de chaque ligne de arg comme argument
counter=1
while read i; do
    N_ARGS=$(echo $i | wc -w)
    if [[ $N_ARGS == 5 ]] ; then
	counter=$((counter+1))
	./pipotron.sh $i
	msg_s="succès: $counter appréciations saisies dans $(pwd)/resultats.txt"
    else
	counter=$((counter+1))
	msg_e="échec appréciations: la ligne $counter comporte $N_ARGS arguments au lieu de 5"
	echo "Nombre arguments élève n:$counter différent de 5"
    fi
done < app_tmp.csv > resultats.txt
rm app_tmp.csv

if [ -z "$msg_e" ]; then
    echo -e "#########################\n$msg_s\n#########################"
else
    echo -e "#########################\n$msg_e\n#########################"
fi
