#!/usr/bin/env bash

#Séparer chaque ligne du fichier en variables nom et note
#Attribuer une appréciation globale à une fourchette de note.
#Attribuer des adjectifs aux observations

##############################
#saisie ou déclaration du prénom
##############################
if [ -z "$1" ]; then
    read -p "Prénom: " prenom
else
    prenom=$1
fi

#############################
#Saisie ou détermination du genre
############################
if [ -z "$2" ]; then
    while [ -z genre ] || [ "$genre" != f ] && [ "$genre" != g ] && [ "$genre" != l ]; do
	read -p 'genre (f=fille; g=garçon; l=ladyboy) : ' genre
    done
else
    genre=$2
fi


############################
# saisie ou boucle d'attente de la note. while:; == while true;
#https://stackoverflow.com/questions/50521798/how-to-check-if-a-number-is-within-a-range-in-shell
###########################
if [ -z "$3" ]; then
    while :; do
	read -p "Note [0-20]: " note
	[[ $note =~ ^[0-9]+$ ]] || { echo "Entrer une note valide"; continue; }
	if ((note >= 0 && note <= 20)); then
	    break
	else
	    echo "La note doit-être comprise entre 0 et 20. Réessayer."
	fi
    done
else
    note=$3
fi
##############################
#saisie bavardages
#############################
if [ -z "$4" ]; then
    while [ -z bav ] || [ "$bav" != o ] && [ "$bav" != n ] && [ "$bav" != d ] && [ "$bav" != D ]; do
	read -p 'Bavardages? [o=oui; n=non; d=discret ; D=dynamique]: ' bav
    done
else
    bav=$4
fi
##############################
#saisie participation
#############################
if [ -z "$5" ]; then
    while [ -z PART ] || [ "$PART" != o ] && [ "$PART" != n ]; do
	read -p 'Participation? [o=oui; n=non]: ' PART
    done
else
    PART=$5
fi
#read -p "Participation? [o/n] "

##############################
#bibliothèque d'appréciations générales selon la note
##############################
declare -a APP_Glt10=("Trimestre en dessous de ses capacités"\
			  "Trimestre en dents de scie"\
			  "Trimestre perfectible"\
			  "Un trimestre manquant de régularité")

declare -a APP_Glt12=("Trimestre satisfaisant"\
			  "Trimestre correct"\
			  "Trimestre convenable")

if [ $bav == "o" ]; then
    APP_Glt14="Bon trimestre sur le plan moteur"
    APP_Glt16="Très bon trimestre sur le plan moteur"
    APP_Glt20="Excellent trimestre sur le plan moteur"
else
    APP_Glt14="Bon trimestre"
    APP_Glt16="Très bon trimestre"
    APP_Glt20="Excellent trimestre"
fi

APP_Glt14="Bon trimestre"
APP_Glt16="Très bon trimestre"
APP_Glt20="Excellent trimestre"


##############################
#Complément genré
##############################

if [ $genre == "f" ]; then
    VERBE="est une élève"
elif [ $genre == "g" ]; then
    VERBE="est un élève"
elif [ $genre == "l" ]; then
    VERBE="est un ladyboy affectueux"
else
    echo "non-binaire?"
fi

###############################
#Génération de la piste de progression genrée
##############################
if [ $genre == "f" ]; then
    PRONOM="Elle"
elif [ $genre == "g" ]; then
    PRONOM="Il"
elif [ $genre == "l" ]; then
    PRONOM="Elle"
fi

declare -a PST=("pourra continuer à progresser en"\
		      "pourra maintenir sa progression en"\
		      "continuera sa progression en"\
		      "poursuivra ses apprentissages en"\
		      "pourra poursuivre dans ses apprentissages en"\
		      "progressera encore davantage en"\
		      "poursuivra sa progression en"\
		      "ira plus loin dans ses apprentissages en"\
		      "maintiendra son niveau de progression en")
PISTE=${PST["$[RANDOM % ${#PST[@]}]"]}
##############################
#Génération d'appréciation générale selon la note.
#https://stackoverflow.com/questions/35623462/bash-select-random-string-from-list
##############################
if [ $note -lt 10 ]; then
    AGG=${APP_Glt10["$[RANDOM % ${#APP_Glt10[@]}]"]}
#    echo ${APP_Glt10["$[RANDOM % ${#APP_Glt10[@]}]"]}
elif [ $note -lt 12 ]; then
    AGG=${APP_Glt12["$[RANDOM % ${#APP_Glt12[@]}]"]}
#    echo ${APP_Glt12["$[RANDOM % ${#APP_Glt12[@]}]"]}
elif [ $note -lt 14 ]; then
    AGG=${APP_Glt14["$[RANDOM % ${#APP_Glt14[@]}]"]}
#    echo $AGG
elif [ $note -lt 16 ]; then
    AGG=${APP_Glt16["$[RANDOM % ${#APP_Glt16[@]}]"]}
#    echo ${APP_Glt16["$[RANDOM % ${#APP_Glt16[@]}]"]}
elif [ $note -lt 21 ]; then
    AGG=${APP_Glt20["$[RANDOM % ${#APP_Glt20[@]}]"]}
#    echo ${APP_Glt20["$[RANDOM % ${#APP_Glt20[@]}]"]}
else
    echo "La note doit être comprise entre 0 et 20"
fi

##############################
#génération d'une banque d'adjectifs pour les bavards
#intégration de la participation seulement pour les élèves à l'écoute
##############################
if [ $bav == "o" ]; then
    if [ $note -le 12 ]; then
       declare -a COMPLEMENT=("capable de bien faire mais qui a tendance à se disperser"\
				  "possédant les capacités pour réussir en EPS mais qui se laisse également distraire"\
				  "qui dispose des moyens pour réussir mais qui peut également se laisser distraire"\
				  "disposant des moyens de réussir en EPS mais a aussi tendance à se disperser")
       COD=${COMPLEMENT["$[RANDOM % ${#COMPLEMENT[@]}]"]}
    elif [ $note -gt 12 ]; then
	declare -a COMPLEMENT=("disposant d'un bon potentiel moteur, mais également d'une tendance à la dispersion"\
				   "possédant des qualités motrices mais également une propension à se laisser disperser"\
				   "disposant d'un certain potentiel moteur, mais aussi d'une tendance à se laisser déconcentrer")
	COD=${COMPLEMENT["$[RANDOM % ${#COMPLEMENT[@]}]"]}
    fi
elif [ $PART == o ]; then
    declare -a COMPLEMENT=("avec une bonne participation en classe"\
			       "avec un bon investissement dans la vie de classe"\
			       "participant très régulièrement"\
			       "avec une bonne participation"\
			       "proposant une bonne participation"\
			       "dont la participation est profitable"\
			       "qui participe très activement en classe"\
			       "participant activement en classe")
    COD=${COMPLEMENT["$[RANDOM % ${#COMPLEMENT[@]}]"]}
fi
##############################
#génération d'une banque d'adjectifs pour les élèves à l'écoute
##############################
#pas de bavardages (différencier selon la note)
if [ $bav == "n" ]; then
    if [ $genre == "g" ] || [ $genre == "l" ]; then
	if [ $note -le 12 ]; then
	    declare -a ADJECTIF=("volontaire"\
				     "soucieux de bien faire"\
				     "persévérant"\
				     "investi")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	else
	    declare -a ADJECTIF=("à l'écoute"\
				     "attentif"\
				     "sérieux,"\
				     "impliqué"\
				     "appliqué"\
				     "persévérant")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	fi
    elif [ $genre == "f" ]; then
	if [ $note -le 12 ]; then
	    declare -a ADJECTIF=("volontaire"\
				     "soucieuse de bien faire"\
				     "persévérante"\
				     "investie")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	else
	    declare -a ADJECTIF=("à l'écoute"\
				     "attentive"\
				     "sérieuse"\
				     "impliquée"\
				     "appliquée"\
				     "persévérante")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	fi
    fi
fi

if [ $bav == "D" ]; then
    ADJ="dynamique"
fi

###############################
# pour les discrets: 
if [ $bav == "d" ]; then
    if [ $note -le 12 ]; then
	if [ $genre == "f" ] ; then
	    declare -a ADJECTIF=("discrète, parfois en retrait,"\
				     "calme, parfois en retrait,"\
				     "calme"\
				     "discrète"\
				     "réservée")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	elif [ $genre == "g" ] ; then
	    declare -a ADJECTIF=("discret, parfois en retrait"\
				     "calme, parfois en retrait"\
				     "calme"\
				     "discret"\
				     "réservé")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	fi
    elif [ $note -gt 12 ]; then
	if [ $genre == "f" ] ; then
	    declare -a ADJECTIF=("discrète, mais à l'écoute,"\
				     "calme, mais attentive,"\
				     "calme, mais à l'écoute"\
				     "réservée, mais attentive")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	elif [ $genre == "g" ] || [ genre == "l" ] ; then
	    declare -a ADJECTIF=("discret, mais à l'écoute,"\
				     "calme, mais attentif"\
				     "calme, mais à l'écoute"\
				     "réservé, mais attentif")
	    ADJ=${ADJECTIF["$[RANDOM % ${#ADJECTIF[@]}]"]}
	fi
    fi
fi

##############################
#génération aléatoire d'adverbes de progression
##############################

if [ $note -le 12 ]; then
    if [ $bav == "o" ]; then
	declare -a ADVERBE=("augmentant son niveau de concentration durant les consignes"\
			    "réduisant les bavardages pendant les phases de regroupement"\
			    "réduisant les bavardages lors des phases de consignes"\
			    "haussant son niveau de concentration pendant les phases d'échange"\
			    "élevant son niveau à l'écoute pendant les leçons"\
			    "étant plus à l'écoute et en réduisant les bavardages"\
			    "augmentant son niveau d'attention lors des consignes")
	ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
    else
	declare -a ADVERBE=("poursuivant les efforts entrepris ce trimestre"\
				"maintenant le cap au prochain trimestre"\
				"continuant dans cette direction pour le prochain cycle")
	ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
    fi
elif [ $note -gt 12 ]; then
    if [ $bav == "o" ]; then
	declare -a ADVERBE=("augmentant son niveau de concentration durant les consignes"\
			    "réduisant les bavardages pendant les phases de regroupement"\
			    "réduisant les bavardages lors des phases de consignes"\
			    "haussant son niveau de concentration pendant les phases d'échange"\
			    "étant davantage à l'écoute pendant les leçons"\
			    "étant plus à l'écoute et en réduisant les bavardages"\
			    "augmentant son niveau d'attention lors des consignes")
	ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
    elif [ $bav == "d" ]; then
	declare -a ADVERBE=("n'hésitant pas à intervenir davantage."\
				"intervenant plus fréquemment à l'oral."\
				"augmentant la fréquence de ses prises de parole")
	ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
    elif [ $bav == "D" ]; then
	declare -a ADVERBE=("poursuivant dans cette direction au prochain cycle."\
				"maintenant le cap au prochain trimestre."\
				"continuant dans cette direction à l'avenir")
	ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
    elif [ $bav == "n" ]; then
	if [ $PART == "n" ]; then
	    declare -a ADVERBE=("participant davantage"\
				    "maintenant le cap au prochain trimestre."\
				    "continuant dans cette direction.")
	    ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}
	    echo -e "$ADV\n"
	else
	    declare -a ADVERBE=("poursuivant dans cette direction au prochain cycle"\
				    "maintenant le cap au prochain trimestre"\
				    "continuant dans cette direction.")
	    ADV=${ADVERBE["$[RANDOM % ${#ADVERBE[@]}]"]}	    
	fi
    fi
fi

echo -e "\n$AGG. $prenom $VERBE $ADJ $COD. $PRONOM $PISTE $ADV.\n"

exit 0

