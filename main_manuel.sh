#!/usr/bin/env bash
echo "
##################################################
#		PIPOTRON			 #
##################################################
"
until [[ $continue == "n" ]]; do
    read -p "Veux-tu continuer à me faire faire ton travail? [o/n]: " continue
    if [[ $continue == "o" ]]; then
	echo "ok on continue... (assisté)"
	/bin/bash `pwd`/pipotron.sh
    elif [[ $continue == "n" ]]; then
	echo -e "\n Allez salut!\n(va bosser...)"
	break
    else
	echo "mauvaise entrée (o: continuer / n: stop)"
    fi
done
exit 0
